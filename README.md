Gizmo
=====

Gizmo - VMware and Red Hat Satellite automation and integration

More documentation: [Gizmo Docs](http://gizmo.moosejudge.com)


Installation
------------

_User Install_

  - Python 3.4+ with virtualwrapper installed
    - Red Hat: `sudo yum install python-virtualenvwrapper`
    - Other: `pip install virtualenvwrapper`
  - Create a Virtual Environment
    - `python -m virtualenv gizmo`
    - `cd gizmo`
    - `source ./bin/activate`
  - Install gizmo
    - `pip install git+https://ugitlab.ohsu.edu/morelanc/py-gizmo.git@latest#egg=gizmo`
  - Create link to bin directory in path
    - `echo $PATH`
    - `ln -s gizmo/bin/gizmo ~/.local/bin/gizmo`

_System Wide Install_

  - Python 2.7+ with virtualwrapper installed
    - Red Hat: `sudo yum install python-virtualenvwrapper`
    - Other: `pip install virtualenvwrapper`
  - Create a Virtual Environment
    - `python -m virtualenv /opt/gizmo`
    - `cd /opt/gizmo`
    - `source ./bin/activate`
  - Install gizmo
    - `pip install git+https://ugitlab.ohsu.edu/morelanc/py-gizmo.git@latest#egg=gizmo`
  - Create link to bin directory in path
    - `ln -s /opt/gizmo/bin/gizmo /usr/local/bin/gizmo`
     
Quick Start
-----------

_Config File_

  - Unix: `~/.config/gizmo/config.yaml`
  - Windows: `%APPDATA%\gizmo\config.yaml`
  - [Gizmo Config](http://gizmo.moosejudge.com/en/latest/config/)
  
_Setup_

Passwords stored in a Gizmo config file must be encoded.

  - Encode password for config file
    - `gizmo setup secret`
    - A '--string password' can be passed leaving the password clear text in history.

_Satellite_ 

  - Show status summary of a Satellite Host
    - `gizmo satellite host --name hostname.example.com status`
  - Show all facts for a Satellite Host
    - `gizmo satellite host --name hostname.example.com facts`
    - Add one or more '-f <fact-name>' to limit facts]
  - Dump the Satellite Host entry (output in YAML)
    - `gizmo satellite host --name hostname.example.com dump`
  - Create column list of hosts using glists
    - `gizmo satellite glist --name host_ids --serach 'lifecycle_environment == DEV'`
    - `gizmo satellite glist --name status --serach 'lifecycle_environment == DEV'`
  - Dump list data with glist in yaml
    - `gizmo satellite glist --name status --serach 'lifecycle_environment == DEV' --yamlout`
  - Write glist output to a file
    - `gizmo satellite glist --name status --serach 'lifecycle_environment == DEV' --yamlout --file /tmp/out_file`
    - `gizmo satellite glist --name host_ids --serach 'lifecycle_environment == DEV' --file /tmp/out_file`
    
_VCenter_

  - Show status of VCenter guest
    - `gizmo vcenter --guest hostname.example.com summary`
  - Change power state of a VCenter guest
    - `gizmo vcenter --guest hostname.example.com power off`
    - `gizmo vcenter --guest hostname.example.com power reset`
  - List snapshots of a VCenter guest
    - `gizmo vcenter --guest hostname.example.com snapshot list`
  - Create a snapshot of a VCenter guest
    - `gizmo vcenter --guest hostname.example.com snapshot create --name THIS_IS_A_SNAPSHOT`
  - Revert to a snapshot of a VCenter guest
    - `gizmo vcenter --guest hostname.example.com power off`
    - `gizmo vcenter --guest hostname.example.com snapshot revert --name THIS_IS_A_SNAPSHOT`
    - `gizmo vcenter --guest hostname.example.com power on`
    - The revert will reset the host, but I always power off first
  - Remove a snapshot of a VCenter guest
    - `gizmo vcenter --guest hostname.example.com snapshot remove --name THIS_IS_A_SNAPSHOT`


[![Documentation Status](https://readthedocs.org/projects/py-gizmo/badge/?version=latest)](http://gizmo.moosejudge.com/en/latest/?badge=latest)
[TOC]

VCenter
=======

Manage Guests in VCenter

    default VM search is by dns (--by dns). If the VM is not running use the uuid (--by uuid)

Commands
--------

_vcenter syntax_:
```
gizmo vcenter [--name PROFILE] COMMAND [ARGS] [--by (dns|ip|uuid)] --guest GUEST COMMAND [ARGS]
gizmo vcenter [--server SERVER --username USER --password PASSWD] --guest GUEST COMMAND [ARGS]
```

Command  | Description
------   | -------------
summary  | Display guest VM summary
power    | Manage power state of guest VM
snapshot | Manage snapshots of guest VM

power command
-------------

_power syntax_:
```
gizmo vcenter --guest GUEST power (on|off|reset) [--yes] [--background]
```

power actions
-------------

Action | Description
------ | -----------
on     | Power on guest VM
off    | Power off guest VM
reset  | Reset power of guest VM

snapshot command
----------------

_snapshot syntax_:
```
gizmo vcenter --guest GUEST snapshot ACTION --name SNAPSHOT [--yes] [--background]
```

snapshot actions
----------------

Action | Argument        | Description
------ | -----------     | -----------
list   |                 | List current guest snapshots
create | --name SNAPSHOT | Create a guest snapshot
revert | --name SNAPSHOT | Revert a guest to snapshot
remove | --name SNAPSHOT | Remove a guest snapshot


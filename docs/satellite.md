[TOC]

Satellite
=========

Pulls data from Red Hat Satellite server for viewing. 


Commands
--------

_satellite syntax_:
```
gizmo satellite [--name PROFILE] COMMAND [ARGS]
gizmo satellite [--urlbase URL --org ORG --username USER --password PASSWD] COMMAND [ARGS]
```

Command | Description
------  | -------------
host    | Get and display satellite data about a host
glist   | Generate a list of satellite hosts based on a search


host command
------------

_host syntax_:
```
gizmo satellite host --name HOST [--glist GLIST] [-f FACT (-f FACT)] ACTION 
gizmo satellite host --name HOST [--glist GLIST] [-f FACT (-f FACT)] status
gizmo satellite host --name HOST [--glist GLIST] [-f FACT (-f FACT)] facts
gizmo satellite host --name HOST [--glist GLIST] [-f FACT (-f FACT)] dump 
```

host actions
------------

Action  | Description
------  | ---------
status  | Summary status of host
dump    | Dump host entry in yaml
facts   | List all facts for a host

glist command
-------------

[glist](glists) | Create and modify glists 

_glist syntax_:
```
gizmo satellite glist --name GLIST --search SEARCH [--file FILE] [--yamlout] [-fs FS]
```

glist options
-------------

Option          | Description
------          | -----------
`--file FILE`   | File to write output to
`-fs FS`        | Field seperator 
`--yamlout`     | Output data in YAML
`--no-facts`    | Ignore fact lookups
`--no-header`   | Don't output the header

included glists
---------------

Name           | Description
------------   | -----------
host_status    | Default list used for status action of host
host_check     | Machine parsable list
host_class     | Host classification by sysowner
host_ids       | Host IDS from Satellite and facts
host_auth      | Host authentication settings 



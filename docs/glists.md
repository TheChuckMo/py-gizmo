[TOC]

GLists
======

Create custom lists within your local configuration file. Using the name of an included list will override that list. 

See the [Configure](config) page for config file locations and other details. 

syntax
------

      glists:
        [LIST-NAME]:
          - [LIST-LINE-1]
          - [LIST-LINE-2]
          - [LIST-LINE-3]

column params
-------------

Params | Usage        | Note
------ | -----        | -----
name   | Unique name  | use in format
label  | Column Label |
field  | Field name   | find field from host dump
format | Formating    | required `'{name}'` 

_format notes_

  - standard python formating - [pyformat](https://pyformat.info/)

example
-------

```yaml
glists:
  host_auth:
    - { name: name, label: Name, field: name, format: '{name:25}' }
    - { name: lifecycle, label: Lifecycle, field: 'content_facet_attributes.lifecycle_environment.name', format: '{lifecycle:10}' }
    - { name: system_auth, label: system_auth, field: 'facts.system_auth', format: '{system_auth:15}' }
    - { name: uadmin, label: uadmin, field: 'facts.uadmin', format: '{uadmin:15}' }
    - { name: cfy_adinfo, label: cfy_adinfo, field: 'facts.cfy_adinfo', format: '{cfy_adinfo:15}' }
    - { name: cfy_domain, label: cfy_domain, field: 'facts.cfy_domain', format: '{cfy_domain:15}' }
    - { name: cfy_zonedn, label: cfy_zonedn, field: 'facts.cfy_zonedn', format: '{cfy_zonedn:30}' }
```

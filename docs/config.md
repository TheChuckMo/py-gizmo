[TOC]

Gizmo Configuration
===================

Configuration files are written in YAML.

locations
---------

OS             | Location
--------       | --------
Linux/Unix/Mac | `~/.config/gizmo/config.yaml`
Windows        | `%APPDATA%\gizmo\config.yaml`

config sample
-------------

_For multi-server entries, the first is default_

```yaml
    ---
    loglevel: warn
    # satellite configuration
    satellite:
      enterprise:
        org: Enterprise
        username: user-admin
        password: **encoded**
        sslVerify: True
        urlbase: https://satellite.example.com
      public:
        org: Public
        username: user-admin
        password: **encoded**
        sslVerify: True
        urlbase: https://satellite.example.com
    # vcenter configuration
    vcenter:
      newvc:
        username: user-admin
        password: **encoded**
        server: vc-msb.example.com
        port: 443
        by: dns
        sslVerify: True
      oldvc:
        username: user-admin
        password: **encoded**
        server: vcenter.example.com
        port: 443
        by: dns
        sslVerify: False
```

encode password
---------------

Passwords in a Gizmo config file must be encoded by gizmo. 
 
`gizmo setup --secret`
 
parameters
----------

Parameter               | Type      | Values                                                    | Default
:---------              |:-----     |:-------                                                   |:--------
loglevel                |string     |info, warn, debug                                          |warn
vcenter                 |hash list  |name: {username, password, server, port, by, sslVerify}    |first entry
vcenter.name            |hash       |username, password, server, port, by, sslVerify            |none
vcenter.name.username   |string     |Username to connect to VCenter as                          |none
vcenter.name.password   |encoded    |Password string encoded by Gizmo	                        |none
vcenter.name.server     |string     |VCenter server name                                        |none
vcenter.name.port       |int        |Port number for vcenter                                    |443
vcenter.name.by	        |string     |uuid, dns, ip - Default method to search by                |dns
vcenter.name.sslVerify  |boolean    |Verify trusted ssl connection                              |True
satellite               |hash list  |name: {org, username, password, sslVerify, urlbase}        |first entry
satellite.name          |hash       |org, username, password, sslVerify, urlbase                |none
satellite.name.org      |string     |Organization within Satellite (ucat, acc, ohsu)            |none
satellite.name.username |string     |username for connecting to Satellite API                   |none
satellite.name.password |encoded    |Password string encoded by Gizmo                           |none
satellite.name.urlbase  |string/url |URL Base of Satellite server (https://server.example.com)  |none
satellite.name.sslVerify|boolean    |Verify trusted ssl connection                              |True



[TOC]

Tools Used in Gizmo
===================

Tools modules and APIs used in Gizmo. 

$ click_
--------
Command line package/framework for Python.

  - [docs](http://click.pocoo.org/6/)
  - [source](https://github.com/pallets/click)

    Click is a Python package for creating beautiful command line interfaces 
    in a composable way with as little code as necessary.  It's the 
    "Command Line Interface Creation Kit".

confuse
-------
Configuration system for python

  - [docs](http://confuse.readthedocs.io/en/latest/)
  - [source](https://github.com/sampsyo/confuse)

    I'm tired of fiddling around with ConfigParser...

    So I'm writing Confuse to magically take care of defaults, overrides, type 
    checking, command-line integration, human-readable errors, and standard 
    OS-specific locations. The configuration files will be based on YAML, which 
    is a great syntax for writing down data.
  
pyVmomi
-------
Python SDK for the VMware vSphere API

  - [api-reference](http://pubs.vmware.com/vsphere-65/index.jsp?topic=%2Fcom.vmware.wssdk.apiref.doc%2Fright-pane.html)
  - [tools](https://github.com/vmware/pyvmomi-tools)
  - [source](https://github.com/vmware/pyvmomi)
  - [community-samples](http://vmware.github.io/pyvmomi-community-samples/)
  
    pyVmomi is the Python SDK for the VMware vSphere API that allows you to manage ESX, ESXi, and vCenter.

Satellite 6.2 REST API 
----------------------
REST API accessed via python requests

  - [api-guide](https://access.redhat.com/documentation/en-us/red_hat_satellite/6.2/html/api_guide/)

    API GUIDE
    RED HAT SATELLITE
    6.2
    REFERENCE DOCUMENTATION FOR USING SATELLITE'S REPRESENTATIONAL STATE TRANSFER (REST) API

Contribute
==========

_Development Requirements Setup_

- Python 2.7
- Python Virtualenv
- Python Virtualenvwrapper

```bash
sudo yum install python
sudo yum install python-virtualenv
sudo yum install python-virtualenvwrapper
#
# source /usr/bin/virtualenvwrapper.sh in profile
#
vi ~/.bash_profile
#
# Virtualenv Wrapper Setup
#
PROJECT_HOME=/path/to/dev_dir
source /usr/bin/virtualenvwrapper.sh
#
# logout and login for changes
#
lsvirtualenv
```
    
_Development Environment_

```bash
cd dev_dir
mkvirtualenv py27v
git clone https://morelanc@ugitlab.ohsu.edu/morelanc/py-gizmo.git py-gizmo
cd py-gizmo
setvirtualenvproject
pip install --upgrade pip
pip install --editable .
```

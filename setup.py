"""
Setup Gizmo
"""
from setuptools import find_packages, setup

dependencies = ['click', 'requests', 'pyvmomi', 'pyVim', 'colorama', 'yamlreader', 'pyYAML', 'confuse', 'six', 'pyVim',
                'future', 'future']

setup(
    name='gizmo',
    version='0.4.0',
    url='https://gitlab.com/TheChuckMo/py-gizmo.git',
    license='GPLv3',

    author='Chuck Moreland',
    author_email='morelanc@ohsu.edu',

    description='Gizmo automation',
    long_description='Simple interface to Satellite and VCenter',
    keywords='vmware vcenter satellite rhel redhat facts',

    packages=find_packages(exclude=['tests']),
    include_package_data=True,

    zip_safe=False,
    platforms='any',
    install_requires=dependencies,

    # Include default config
    package_data={
        'config_default': ['gizmo/config_default.yaml'],
    },

    # Setup console script
    entry_points={
        'console_scripts': [
            'gizmo = gizmo.app:cli',
        ],
    },

    # sometimes we need more...
    extras_require={
      'dev': ['wheel'],
      'test': ['tests'],
      'build': ['setuptools', 'wheel', 'twine'],
    },

    classifiers=[
        # As from http://pypi.python.org/pypi?%3Aaction=list_classifiers
        # 'Development Status :: 1 - Planning',
        # 'Development Status :: 2 - Pre-Alpha',
        'Development Status :: 3 - Alpha',
        # 'Development Status :: 4 - Beta',
        # 'Development Status :: 5 - Production/Stable',
        # 'Development Status :: 6 - Mature',
        # 'Development Status :: 7 - Inactive',
        'Environment :: Console',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: POSIX',
        'Operating System :: MacOS',
        'Operating System :: Unix',
        # 'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python :: 3',
        'Topic :: System :: Systems Administration',
        'Topic :: System :: Installation/Setup',
    ]
)

"""AppConfig Class and utilities"""
from __future__ import print_function

import base64
import logging
from datetime import datetime
from collections import defaultdict

import click
import confuse

logger = logging.getLogger(__name__)


class AppConfig(object):
    """AppConfig sane defaults for app configuration"""

    _SECRETS_ = ['password']
    _FLATTEN_ = ['vcenter', 'satellite']
    _DEFAULTS_ = {
        'sslVerify': True,
    }

    def __init__(self, app, **kwargs):
        """Gizmo gets configuration 
        
        required arguments:
            server:    None    (str): VCenter server
            
        required arguments:
            app:        None    (str): application name
            
        keyword arguments:
            defaults:   None    (dict): dictionary of defaults
    
        Returns:
            gizmo.utils.AppConfig object
        """

        self.app = app
        logger.debug('app config name {}'.format(self.app))

        self._config = confuse.Configuration(self.app, 'gizmo')
        logger.debug('confuse config created {}'.format(self._config.keys()))

        self._defaults = kwargs.get('defaults', {})
        logger.debug('confuse config defaults {}'.format(self._defaults))

        # list of secrets in config file
        self.secrets = kwargs.get('secrets', self._SECRETS_)
        logger.debug('secrets list {}'.format(self.secrets))

        # list of groups to flatten
        self.flatten = kwargs.get('flatten', self._FLATTEN_)
        logger.debug('flatten list {}'.format(self.flatten))

    @property
    def default_map(self):
        """default_map for click cli"""
        _default_map = dict()

        _default_map.update({'loglevel': self._config['loglevel'].get(str)})

        _default_map.update({'satellite': self.satellite()})

        _default_map.update({'vcenter': self.vcenter()})

        return _default_map

    def satellite(self, **kwargs):
        """return satellite config entry"""
        _name = kwargs.get('name')
        _entry = dict()

        logger.debug('satellite entry: {}', _name)
        if self._config['satellite'].exists():
            if not _name:
                _name = next(self._config['satellite'].all_contents())
        else:
            _entry.update({'error': 'no entry'})
            return _entry

        for key in self._config['satellite'][_name].keys():
            if key in self.secrets:
                _entry.update({key: self.get_secret(self._config['satellite'][_name][key].get())})
            else:
                _entry.update({key: self._config['satellite'][_name][key].get()})

        logger.debug('satellite({}): {}'.format(_name, _entry))

        return _entry

    def vcenter(self, **kwargs):
        """return vcenter config entry"""
        _name = kwargs.get('name')
        _entry = dict()

        logger.debug('vcenter entry: {}', _name)
        if self._config['vcenter'].exists():
            if not _name:
                _name = next(self._config['vcenter'].all_contents())
        else:
            _entry.update({'error': 'no entry'})
            return _entry

        for key in self._config['vcenter'][_name].keys():
            if key in self.secrets:
                _entry.update({key: self.get_secret(self._config['vcenter'][_name][key].get())})
            else:
                _entry.update({key: self._config['vcenter'][_name][key].get()})

        logger.debug('vcenter({}): {}'.format(_name, _entry))

        return _entry

    def glists(self, **kwargs):
        """list formatting

        Template for building host lists

        Required parameters:
            :name:  name of the list
        """
        _name = kwargs.get('name')

        if not _name:
            _name = next(self._config['glists'].all_contents())

        logger.debug('glists({})'.format(_name))

        _list = defaultdict()
        _list['fields'] = list()
        _list['header'] = dict()
        _list['format'] = list()

        if self._config['glists'][_name].exists():
            for item in self._config['glists'][_name].get():
                _list['format'].append(item.get('format'))
                _list['fields'].append((item.get('name'), item.get('field')))
                _list['header'].update({item.get('name'): item.get('label')})

        return _list

    @staticmethod
    def get_secret(secret):
        """Gets secret and decodes"""
        try:
            return base64.b64decode(secret)
        except:
            print('failed to decode password')
            raise

    @staticmethod
    def make_secret(secret):
        """weak way to obscure password"""
        return base64.b64encode(secret)


def echo_header(title, width=65, underline=True, bold=True, fg='magenta'):
    """print header line for vcenter"""
    click.secho('{title:{width}}'.format(title=title, width=width), underline=underline, fg=fg, bold=bold)


def echo_line(name, value, blink=False, bold=False, underline=False, fg=None, name_width=20, value_width=35):
    """print line for vcenter"""
    if type(value) is datetime.date:
        click.secho('{name:{name_width}}| {value:%Y-%m-%d %H:%M}'.format(name=name,
                                                                         value=value,
                                                                         name_width=name_width),
                    fg=fg, underline=underline, blink=blink, bold=bold)
    else:
        click.secho('{name:{name_width}}| {value:{value_width}}'.format(name=name,
                                                                        value=str(value),
                                                                        name_width=name_width,
                                                                        value_width=value_width),
                    fg=fg, underline=underline, blink=blink, bold=bold)


def echo_snapshots(snapshots, **kwargs):
    """
    echo snapshots to the screen
   
    arguments:
     snapshots: list of snapshots
     
    keyword arguments:
     header: bool (true) print the header
     header_color: string of header text color (magenta)
     line_color: string of line text color (blue)
    
    snapshot hash:  
    [ 
      {
        'id': int,
        'parent': str,
        'name': str,
        'state': str,
        'description': str,
        'createTime': DateTime,
        'current': bool,
        'object': vim.Snapshot,
      },
    ]
    """

    header = bool(kwargs.get('header', True))
    header_color = kwargs.get('header_color', 'magenta')
    line_color = kwargs.get('line_color', 'blue')

    date_format = '%Y-%m-%d %H:%M'
    line_format = '{id:<3}| {created:{dfmt}} |{x:^1}| {name:40} {parent:40}'

    # do we have snapshots?
    if type(snapshots) is not list or len(snapshots) == 0:
        click.secho('no snapshots found')
        return

    # should we print a header?
    if header:
        click.secho(line_format.format(id='ID',
                                       created='Created',
                                       dfmt='16',
                                       x='X',
                                       name='Snapshot',
                                       parent='Parent'), fg=header_color, underline=True)

    # print them snapshots
    for snap in snapshots:
        # is this our current snapshot?
        current = ' '
        if snap.get('current'):
            current = 'X'

        click.secho(line_format.format(id=snap.get('id'),
                                       created=snap.get('createTime'),
                                       dfmt=date_format,
                                       x=current,
                                       name=snap.get('name'), parent=snap.get('parent')), fg=line_color)


def echo_vm_summary(vm):
    """Gizmo prints VM Summary"""
    echo_header('VM/Guest Status')
    echo_line('OverAll Status', vm.overall_status, fg=vm.overall_status)
    echo_line('Tools Status', vm.tools_status, fg='blue')
    echo_line('VM State', vm.vm_state, fg='blue')
    echo_line('Guest State', vm.guest_state, fg='blue')

    if vm.alarms:
        echo_header('Alarms')
        for alarm in vm.alarms:
            echo_line(alarm['name'], alarm['status'], fg='red', blink=True)

    if vm.tasks:
        echo_header('Tasks')
        for task in vm.tasks:
            if task['progress'] is not None:
                status = '{}-{}%'.format(task['state'], task['progress'])
            else:
                status = '{}'.format(task['state'])

            echo_line(task['description'], status)

    echo_header('VM Summary')
    echo_line('UUID', vm.uuid)
    echo_line('VM Name', vm.vm_name)
    echo_line('Hostname', vm.hostname)
    echo_line('IP Address', vm.ipaddress)
    echo_line('Boot Time', vm.boot_time)
    echo_line('Tools version', vm.tools_version)
    echo_line('Guest Name', vm.guest_name)
    echo_line('VM Path', vm.path_name)
    echo_line('ESX Host', vm.esx_host)

    echo_header('Hardware')
    echo_line('NUM CPU', vm.num_cpu)
    echo_line('Core/Socket', vm.num_cores_per_socket)
    echo_line('CPU Hot Add', vm.cpu_hot_add)
    echo_line('CPU Hot Remove', vm.cpu_hot_remove)
    echo_line('Memory MB', vm.memory_mb)
    echo_line('Memory Hot Add', vm.memory_hot_add)

    echo_header('end')

    # useless
    # echo_line('Last Modified', vm.last_modified)

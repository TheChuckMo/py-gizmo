"""
Gizmo App Entry
"""
from __future__ import print_function

import logging
import os

import click
import yaml

from gizmo.satellite import SatConnect
from gizmo.utils import echo_vm_summary, echo_snapshots, AppConfig
from gizmo.vcenter import VCenterConnect, VCenterVMs

# For later use - reading hiera and job configurations in YAML
# from yamlreader import yaml_load

# global defaults
APP = 'gizmo'
LOGGER_LEVELS = ['debug', 'info', 'warn', 'error']
LOCAL_USER = os.environ.get('USER', '')

# logger setup
logger = logging.getLogger(APP)

logger_handler = logging.StreamHandler()
logger_handler.setLevel(logging.DEBUG)
logger_formatter = logging.Formatter('%(asctime)s (%(name)s/%(module)s) [%(levelname)s] %(message)s')
logger_handler.setFormatter(logger_formatter)

logger.addHandler(logger_handler)
logger.setLevel(logging.WARN)

# read app config files
cfg = AppConfig(APP)
# pass_cfg = click.make_pass_decorator(AppConfig)

context_settings = {
    'default_map': cfg.default_map
}


# CLICK CLI Group
@click.group(context_settings=context_settings, help='Gizmo is automation')
@click.option('--loglevel', default='warn', type=click.Choice(LOGGER_LEVELS), help='Log level displayed')
@click.version_option()
def cli(loglevel):
    """Gizmo is automation - cli"""

    # set loggin level
    lognum = getattr(logging, loglevel.upper(), None)
    logger.setLevel(level=lognum)
    logger.info('logging level set to ({num}:{level})'.format(num=lognum, level=loglevel))


# Gizmo Configs
@cli.command(help='Gizmo setup tools')
@click.option('--string', type=str, help="String to act on")
@click.argument('action')
@click.pass_context
def setup(ctx, action, string):
    """Manage Gizmo configuration
  
    \b   
    secret: encode string like password for storing in configuration file
    """
    if action == 'show_raw':
        logger.debug('context default_map {}'.format(cfg.default_map))
        logger.debug('config directory {}'.format(cfg._config.config_dir()))
        for content in cfg._config.all_contents():
            logger.debug('config all_contents {}'.format(content))
    elif action == 'secret':
        if string is None:
            string = click.prompt('String to make secret', hide_input=True, confirmation_prompt=True)
        click.echo('secret: {}'.format(cfg.make_secret(string)))


# VCenter entry
@cli.group('vcenter')
@click.option('--name', type=str, help='VCenter profile name')
@click.option('--server', type=str, help='VCenter server name')
@click.option('--port', default=443, type=int, help='Port number')
@click.option('--username', type=str, help='Username to connect as')
@click.option('--password', type=str, help='User password')
@click.option('--guest', prompt=True, type=str, help='Guest search string')
@click.option('--by', default='dns', type=click.Choice(['dns', 'ip', 'uuid']), help='VM Guest search type')
@click.option('--sslVerify', 'sslVerify', is_flag=True, default=True, help='Verify ssl Connection')
@click.pass_context
def vcenter_cli(ctx, name, server, port, username, password, guest, by, sslVerify):
    """Manage Guests in VCenter
  
  
    Perform actions on VMs within vcenter. 
    
    \b   
    Notes: Gizmo's default VM search is by dns (--by dns). If the VM is not running use the uuid (--by uuid)
        
    Examples (assumes site or user defaults configured):
        gizmo vcenter --guest hostname.domain.com summary
        gizmo vcenter --by ip --guest hostname.domain.com snapshot list
        gizmo vcenter --by uuid --guest 887219-2378543-23459 power off
        
    """
    # did we get a profile name?
    if name:
        username = cfg.vcenter(name=name).get('username', username)
        password = cfg.vcenter(name=name).get('password', password)
        server = cfg.vcenter(name=name).get('server', server)
        port = cfg.vcenter(name=name).get('port', port)
        sslVerify = cfg.vcenter(name=name).get('sslVerify', sslVerify)

    # do we have a server name?
    if not server:
        server = click.prompt('VCenter server name', err=True)

    # do we have a password?
    if not password:
        password = click.prompt('VCenter {} password'.format(username), hide_input=True, show_default=False, err=True)

    # create vcenter session (is NOT connected)
    session = VCenterConnect(username=username, password=password, server=server, port=port, sslVerify=sslVerify)

    # add vcenter to context object
    ctx.obj = {
        'vcenter': {
            'session': session,
            'search': guest,
            'by': by,
        },
    }
            #'guest': guest,
            #'server': server,
            #'port': port,
            #'username': username,
            #'password': password,
            #'sslVerify': sslVerify,

# VCenter VM summary
@vcenter_cli.command()
@click.pass_context
def summary(ctx):
    """Show guest summary"""

    with VCenterVMs(**ctx.obj.get('vcenter')) as vm:
        echo_vm_summary(vm)


# VCenter VM power
@vcenter_cli.command()
@click.option('--yes', 'confirm', is_flag=True, default=True, help='Auto confirm prompts')
@click.option('--background', 'background', is_flag=True, default=False, help='Run jobs in background')
@click.argument('action')
@click.pass_context
def power(ctx, action, confirm, background):
    """Manage power state of VM
   
    To power on a VM you must use the uuid.
            
    \b
    ACTIONs: [on, off, reset]
    """
    with VCenterVMs(**ctx.obj.get('vcenter')) as vm:
        if confirm:
            click.confirm('power {action} {by}:{guest} ({vm_name})'.format(action=action,
                                                                           by=ctx.obj.get('vcenter')['by'],
                                                                           guest=ctx.obj.get('vcenter')['guest'],
                                                                           vm_name=vm.vm_name), abort=True)

        click.echo('{by}({guest}): {vm_name} {vm_state}'.format(by=ctx.obj.get('vcenter')['by'],
                                                                guest=ctx.obj.get('vcenter')['guest'],
                                                                vm_name=vm.vm_name,
                                                                vm_state=vm.vm_state))
        vm.power(action, background=background)
        click.echo('{by}({guest}): {vm_name} {vm_state}'.format(by=ctx.obj.get('vcenter')['by'],
                                                                guest=ctx.obj.get('vcenter')['guest'],
                                                                vm_name=vm.vm_name,
                                                                vm_state=vm.vm_state))


# VCenter VM snapshot
@vcenter_cli.command()
@click.option('--yes', 'confirm', is_flag=True, default=True, help='Auto confirm prompts')
@click.option('--background', 'background', is_flag=True, default=False, help='Run jobs in background')
@click.option('--name', help='Name of Snapshot')
@click.argument('action')
@click.pass_context
def snapshot(ctx, action, name, confirm, background):
    """Manage snapshots of VM
    
    A revert will power off the vm and NOT power it back on. 

    \b
    ACTIONs:
        list:      list VM snapshots
        create:    create a VM snapshot (--name)
        revert:    revert to a VM snapshot (--name)
        remove:    remove a snapshot (--name)
    """
    actions = ('list', 'create', 'revert', 'remove')
    if action not in actions:
        click.secho('{}: invalid action'.format(action), fg='yellow')
        click.secho('valid actions: {}'.format(', '.join(actions)))
        exit(1)

    with VCenterVMs(**ctx.obj.get('vcenter')) as vm:
        if action != 'list':
            if not name:
                # some one forget to provide a name
                name = click.prompt('Name of Snapshot', err=True)

            # confirm and perform action
            if confirm:
                click.confirm('{} snapshot {} on {}'.format(action, name, vm.vm_name), abort=True, )

            click.secho('{action} snapshot {name} on {vm_name}'.format(action=action,
                                                                       name=name,
                                                                       vm_name=vm.vm_name))

            vm.vm_snapshot(action=action, name=name, background=background)

        # print the list of snapshots
        click.secho('{len} Snapshots on {vm_name}({hostname})'.format(len=len(vm.snapshots), vm_name=vm.vm_name,
                                                                      hostname=vm.hostname))
        if len(vm.snapshots) > 0:
            echo_snapshots(vm.snapshots)


# Satellite entry
@cli.group('satellite')
@click.option('--name', type=str, help='Satellite profile name')
@click.option('--urlbase', type=str, help='Satellite urlBase')
@click.option('--org', type=str, help='Satellite Organization')
@click.option('--username', type=str, help='Username to connect as')
@click.option('--password', type=str, help='Users password')
@click.option('--sslVerify', 'sslVerify', is_flag=True, default=True, help='Verify ssl Connection')
@click.pass_context
def satellite_cli(ctx, name, username, password, urlbase, org, sslVerify):
    """Satellite 6 actions
    
    Perform actions within Satellite
   
    \b
    required arguements:
        name:   None    Satellite Profile name
    """
    if name:
        username = cfg.satellite(name=name).get('username', username)
        password = cfg.satellite(name=name).get('password', password)
        urlbase = cfg.satellite(name=name).get('urlbase', urlbase)
        org = cfg.satellite(name=name).get('org', org)
        sslVerify = cfg.satellite(name=name).get('sslVerify', sslVerify)

    if not urlbase:
        urlbase = click.prompt('Satellite URL Base', err=True)

    if not username:
        username = click.prompt('Satellite user name', err=True)

    if not password:
        password = click.prompt('Satellite {} password'.format(username), hide_input=True, show_default=False, err=True)

    # create satellite connection (does NOT connect yet)
    # session = SatConnect(urlbase=urlbase, username=username, password=password, org=org, sslVerify=sslVerify)

    # add satellite to context object
    #        'session': session,
    ctx.obj = {
        'satellite': {
            'urlbase': urlbase,
            'username': username,
            'password': password,
            'org': org,
            'sslVerify': sslVerify,
        },
    }


# Satellite hosts
@satellite_cli.command('host')
@click.option('--name', prompt=True, help='Satellites know host name or id')
@click.option('--glist', default='host_status', help='List to user for status')
@click.option('--facts', '-f', multiple=True, help='fact(s) to show, can be used multiple times')
@click.argument('action')
@click.pass_context
def sat_host(ctx, name, glist, facts, action):
    """Manage hosts in Satellite
    
    View and interact with Satellite host entries
    
    \b
    ACTIONs:
        dump:   Dump host entry in yaml format
        facts:  List all facts for a host
        status: Summary status of host
    """
    _line = '{param}: {value}'
    with SatConnect(**ctx.obj.get('satellite')) as sat:
        if action == 'dump':
            click.echo(yaml.safe_dump(sat.get_host_by_id(name), default_style=False))

        if action == 'facts':
            host_facts = sat.get_host_facts(name)
            if len(facts) == 0:
                fact_list = host_facts.keys()
            else:
                fact_list = facts

            for fact in fact_list:
                if fact in host_facts:
                    click.echo(_line.format(param=fact, value=host_facts[fact]))

        if action == 'status':
            name = sat.get_host_by_id(name)
            _status_list = cfg.glists(name=glist)
            host_list = sat.build_host_list(name, _status_list)

            for line in host_list:
                for field in _status_list['fields']:
                    click.echo('{:35}:{}'.format(_status_list['header'].get(field[0]), line.get(field[0])))


# Satellite glists
@satellite_cli.command('glist')
@click.option('--name', prompt=True, help='Gizmo List')
@click.option('--file', default='-', type=click.Path(writable=True, allow_dash=True, dir_okay=False), help='Filename')
@click.option('--search', prompt=True, help='Search string for list')
@click.option('-fs', default='', help='Field separator')
@click.option('--yamlout', is_flag=True, default=False, help='Print as YAML')
@click.option('--no-header', is_flag=True, default=False, help='Print Column Headers')
@click.option('--no-facts', is_flag=True, default=False, help='Print Column Headers')
@click.pass_context
def sat_glist(ctx, name, file, fs, search, yamlout, no_header, no_facts):
    """Gizmo Lists

    Create host lists

    \b
    """
    _list = cfg.glists(name=name)
    _format_line = '{}\n'.format(fs.join(_list['format']))
    outfile = click.open_file(file, 'w')

    if not no_header:
        click.echo('# List name: {}'.format(name))
        click.echo('# Search string: \'{}\''.format(search))

    with SatConnect(**ctx.obj.get('satellite')) as sat:
        # get hosts from search
        hosts = sat.get_host_by_search(search)

        # add facts
        with click.progressbar(hosts) as bar:
            for host in bar:
                if not no_facts:
                    host.update({'facts': sat.get_host_facts(host['id'])})
                host.update({'owner': sat.get_user_by_id(host['owner_id'])})

        # Build our list
        _host_list = sat.build_host_list(hosts, _list)

        click.echo('# {} hosts returned'.format(len(_host_list)))

        if yamlout:
            outfile.write(yaml.safe_dump(_host_list))
        else:
            # header
            if not no_header:
                outfile.write(_format_line.format(**_list['header']))

            # lines
            for _line in _host_list:
                outfile.write(_format_line.format(**_line))

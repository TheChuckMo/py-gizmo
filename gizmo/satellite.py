"""gizmo satellite module"""
import future.standard_library
import requests
import logging
import os
future.standard_library.install_aliases()
# noinspection PyPep8
from urllib.parse import urljoin, urlencode

# from urllib.error import HTTPError

logger = logging.getLogger(__name__)


# logger = logging.getLogger()
# logger_handler = logging.StreamHandler()
# logger_handler.setLevel(logging.DEBUG)
# logger_formatter = logging.Formatter('%(asctime)s (%(name)s/%(module)s) [%(levelname)s] %(message)s')
# logger_handler.setFormatter(logger_formatter)
# logger.addHandler(logger_handler)
# logger.setLevel(logging.DEBUG)

# todo: host information, overview, erratta, currently scheduled patching
# todo: content-view publishing
# todo: composite-content-view chain publishing
# todo: schedule system patching
# todo: schedule system reboot


class SatConnectInvalid(Exception):
    pass


class SatConnectError(Exception):
    """Custom Satellite Connection Exception"""
    pass


# Satellite Connection Objec
class SatConnect(object):
    """Red Hat Satellite 6 Connection"""

    _SATELLITE_VERSIONS_ = ['6.2.10']
    """Satellite versions verified"""

    _POST_HEADERS_ = {'content-type': 'application/json'}
    """Default post headers"""

    _SAT_CERT_ = '/etc/rhsm/ca/katello-server-ca.pem'
    """Default Satellite Server Cert File Location"""

    _SSLVERIFY_ = True
    """Default SSL_VERIFY"""

    _DEF_SPLITTER_ = '.'
    """Default Splitter"""

    _API_ = {
        'katello': '/katello/api',
        'satellite': '/api',
    }
    """APIs katello and satellite """

    _PATHS_ = {
        'org': {
            'list': '{katello}/organizations',
            'show': '{katello}/organizations/{org_id}',
            'update': '{katello}/organizations/{org_id}',
            'create': '{katello}/organizations',
            'delete': '{katello}/organizations/{org_id}',
        },
        'hosts': {
            'list': '{satellite}/organizations/{org_id}/hosts',
            'host': '{satellite}/hosts/{host_id}',
            'update': '{satellite}/hosts',
            'create': '{satellite}/hosts',
            'status': '{satellite}/hosts/{host_id}/status',
            'config': '{satellite}/hosts/{host_id}/status/{type}',
            'facts': '{satellite}/hosts/{host_id}/facts',
        },
        'users': {
            'list': '{satellite}/users',
            'show': '{satellite}/users/{user_id}',
        }
    }
    """PATHS Variables
    {urlBase} - urlbase of satellite server
    {katello} - katello api path
    {satellite} - satellite api path
    {org_id} - Organization ID - Org Label also works
    {host_id} - Host ID - Or the name of the host known to satellite
    {type} - I don't know and I'm not using it yet
    
    list - GET list of objects
    show - GET object details
    update - PUT update an object
    delete - DELETE an object
    create - POST create an object
    """

    def __init__(self, urlbase, org, username, password, **kwargs):
        """Red Hat Satellite 6 Connect
   
        Red Hat Satellite Server Connection Object
        
        Required parameters:
            :urlbase:   base url for Red Hat satellite server 'https://example.com'
            :org:       Satellite Organization for connection
            :username:  username for Satellite connection
            :password:  password for Satellite connection
            
        Keyword optionals:
            :sslverify: default True
            :katello:   url path to Katello API - default '/katello/api/'
            :satellite: url path to Satellite API - default '/api/'
        """

        self.urlBase = urlbase
        """Base URL of Satellite server (required)"""
        logger.debug('urlbase {}'.format(self.urlBase))

        self.org = org
        """Satellite organization id/name (required)"""
        logger.debug('org {}'.format(self.org))

        self.api = {
            'katello': '{api}'.format(api=kwargs.get('katello', self._API_.get('katello'))),
            'satellite': '{api}'.format(api=kwargs.get('satellite', self._API_.get('satellite'))),
            'org_id': self.org
        }
        """API Dictionary"""
        logger.debug('api {}'.format(self.api))

        self.username = username
        self.password = password
        self.auth = (self.username, self.password)
        logger.debug('auth {}'.format(self.auth))

        self.hosts = list()
        """List of host objects"""

        self.users = dict()
        """Dictionary of users"""

        self.raw_sat_cert = kwargs.get('sat_cert', self._SAT_CERT_)
        self.raw_sslverify = kwargs.get('sslVerify', self._SSLVERIFY_)

        if self.raw_sslverify and os.path.exists(self.raw_sat_cert):
            self.sslVerify = self.raw_sat_cert
        else:
            self.sslVerify = False
        logger.debug('sslVerify {}'.format(self.sslVerify))

        self.POST_HEADERS = kwargs.get('POST_HEADERS', self._POST_HEADERS_)
        logger.debug('POST_HEADERS {}'.format(self.POST_HEADERS))

        self.paths = kwargs.get('paths', self._PATHS_)
        logger.debug('paths {}'.format(self.paths))

        logger.debug('SatConnect created {}'.format(self.__str__()))

    def __str__(self):
        return '< SatConnect {url}:({org}/{username}) >'.format(url=self.urlBase, org=self.org,
                                                                username=self.username)

    def __repr__(self):
        return 'SatConnect(\'{url}\', \'{org}\', \'{username}\', \'****\')'.format(url=self.urlBase,
                                                                                   org=self.org,
                                                                                   username=self.username)

    def __enter__(self):
        logger.debug('enter SatConnect with')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.debug('exit SatConnect with')

    def get_results(self, url, **kwargs):
        """get_results from satellite server
        
        will return all pages of a request 
        """

        # This should be done differently!!!
        # Red Hat recommendation is under 50 per_page
        # we should loop on 50 per_page until all results returned!
        url_params = kwargs.get('url_params', {'per_page': 500})
        logger.debug('get_results url {}'.format(url))
        logger.debug('get_results urlparams {}'.format(url_params))

        _json = self.get_json(url, url_params=url_params)

        logger.debug('json {}'.format(_json))

        if 'results' in _json:
            logger.debug('returning results')
            return _json.get('results')
        elif 'error' in _json:
            logger.debug('returning error')
            return _json.get('error')
        else:
            logger.debug('returning json')
            return _json

    def get_json(self, url, **kwargs):
        """Get json from server
       
        Get request to satellite server returning json data. To query entries.
         
        Required parameters:
            :url:   Full url to get json from
        """
        url_params = kwargs.get('url_params', {'per_page': 500})

        if len(url_params) > 0:
            full_url = '{}?{}'.format(url, urlencode(url_params))
        else:
            full_url = '{}'.format(url)

        logger.debug('get url {}'.format(full_url))

        _resp = requests.get(full_url, auth=self.auth, verify=self.sslVerify)
        logger.debug('resp {}'.format(_resp))

        if _resp.ok:
            return _resp.json()
        else:
            return _resp

    def put_json(self, url, url_params, data):
        """Put json to server with url
               
        Put request to satellite server with json data. For updating an entry.
         
        Required parameters:
            :url:   Full url to get json from
            :json:  json data for put request
        """
        logger.debug('url {}'.format(url))

        _resp = requests.put(url,
                             auth=self.auth,
                             verify=self.sslVerify,
                             data=data,
                             headers=self.POST_HEADERS)

        if _resp.ok:
            return _resp.json()
        else:
            raise SatConnectError(_resp)

    def post_json(self, url, urlparams, data):
        """Post json to server with url
        
        Post request to satellite server with json data. For creating an entry.
         
        Required parameters:
            :url:   Full url to get json from
            :json:  json data for put request
        """
        logger.debug('url {}'.format(url))

        _resp = requests.post(url,
                              auth=self.auth,
                              verify=self.sslVerify,
                              data=data,
                              headers=self.POST_HEADERS)

        if _resp.ok:
            return _resp.json().get('results')
        else:
            raise SatConnectError(_resp)

    def build_hosts(self):
        """get host list for organization

        :satellite: api.satellite
        :org_id: api.org
        """
        url = urljoin(self.urlBase, self.paths['hosts']['list'].format(**self.api))
        logger.debug('host_list url {}'.format(url))

        self.hosts = self.get_results(url)

    def get_host_by_search(self, search):
        """get detailed information on host
       
        Does not include facts or owner detail 
        """
        _data = self.api
        url_params = {'search': search, 'per_page': 500}

        url = urljoin(self.urlBase, self.paths['hosts']['list'].format(**_data))
        logger.debug('host url {}'.format(url))

        return self.get_results(url, url_params=url_params)

    def get_user_by_id(self, user_id):
        """get detailed information on host"""
        _data = self.api
        _data.update({'user_id': user_id})

        if user_id in self.users:
            return self.users.get(user_id)

        url = urljoin(self.urlBase, self.paths['users']['show'].format(**_data))
        logger.debug('user url {}'.format(url))

        _user = self.get_results(url)
        self.users.update({user_id: _user})

        return _user

    def get_host_by_id(self, host_id):
        """get detailed information on host"""
        _data = self.api
        _data.update({'host_id': host_id})

        url = urljoin(self.urlBase, self.paths['hosts']['host'].format(**_data))
        logger.debug('host url {}'.format(url))

        _host = self.get_results(url)
        if 'owner_id' in _host:
            _host.update({'owner': self.get_user_by_id(_host['owner_id'])})

        return _host

    def get_host_facts(self, host_id):
        """get facts about host

        params:
            :hostid: Host id from satellite (satellite host name works also)

        :api: api.satellite
        :org_id: api.org
        """
        _data = self.api
        _data.update({'host_id': host_id})

        url = urljoin(self.urlBase, self.paths['hosts']['facts'].format(**_data))
        logger.debug('host_facts url {}'.format(url))

        _resp = self.get_results(url)
        if type(_resp) is dict and len(_resp) == 1:
            return _resp
        else:
            logger.debug('more than one key found? {}'.format(type(_resp)))
            return _resp

    def build_host_list(self, hosts, glist, **kwargs):
        """build a list of hosts with specified parameters"""
        _list = glist['fields']
        _splitter = kwargs.get('splitter', self._DEF_SPLITTER_)
        _host_list = list()

        if type(hosts) is dict:
            hosts = [hosts]

        for host in hosts:
            _line = dict()
            for item in _list:
                _name = item[0]
                _lookup = item[1]

                if _splitter in _lookup:
                    digs = _lookup.split(_splitter)
                    _value = host
                    for dig in digs:
                        if dig in _value:
                            _value = _value[dig]

                    if not isinstance(_value, dict):
                        _line.update({_name: _value})
                    else:
                        _line.update({_name: None})
                else:
                    _line.update({_name: host.get(_lookup)})

            _host_list.append(_line)

        return _host_list

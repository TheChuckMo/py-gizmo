"""gizmo vcenter module"""
import logging
import ssl

from pyVim.connect import Disconnect, SmartConnect
from pyVim.task import WaitForTask

# from pyVmomi import vim
# from pyVmomi import vmodl

logger = logging.getLogger(__name__)


class VCenterConnectError(Exception):
    pass


class VCenterConnectInvalid(Exception):
    pass


class VCenterConnect(object):
    def __init__(self, username, password, server, **kwargs):
        """VMWare VCenter Connect
    
        Prepare connection to VCenter. Use `connect` to open the connection.
    
        required arguments:
            username:  None    (str): VCenter username
            password:  None    (str): VCenter password
            server:    None    (str): VCenter server
            
        keyword arguments:
            port:      443     (int): VCenter port
            sslVerify: True    (bool): Verify SSL during connect
    
        Returns:
            gizmo.vmware.VCenterConnect object
        """
        self.username = username
        logger.debug(('username {}'.format(self.username)))

        self.password = password
        logger.debug(('password {}'.format(self.password)))

        self.server = server
        logger.debug(('server {}'.format(self.server)))

        self.port = kwargs.get('port', 443)
        logger.debug(('port {}'.format(self.port)))

        self.sslVerify = bool(kwargs.get('sslVerify', True))
        logger.debug('sslVerify {}'.format(self.sslVerify))

        self.connected = False

        logger.debug('VCenterConnect created {}'.format(self.__str__()))

    def __repr__(self):
        return 'VCenterConnect(username=\'{username}\', password=\'*****\', server=\'{server}\', port={port})' \
            .format(username=self.username, server=self.server, port=self.port)

    def __str__(self):
        return '< VCenterConnect {username}:{server}:{port} >'. \
            format(username=self.username, server=self.server, port=self.port)

    def connect(self):
        """Make connection to VCenter"""
        if self.sslVerify:
            sslContext = None
            logger.debug('sslVerify {} (ssl verified)'.format(self.sslVerify))
        else:
            sslContext = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
            sslContext.verify_mode = ssl.CERT_NONE
            logger.debug('sslVerify {} (ssl NOT verified)'.format(self.sslVerify))
            logger.warning('sslVerify {} (ssl NOT verified)'.format(self.sslVerify))

        # try:
        self._ServiceInstance = \
            SmartConnect(host=self.server, port=int(self.port), user=self.username, pwd=self.password,
                         sslContext=sslContext)
        # except:
        #    raise VCenterConnectError(
        #        'failed to connect {server}:{port} as {username}'.format(server=self.server, port=self.port,
        #                                                                 username=self.username))

        self.connected = True
        logger.debug('vcenter connected {}'.format(self.__str__()))

    def vm_byDNS(self, name):
        logger.debug('find VM by dns {}'.format(name))
        return self._ServiceInstance.content.searchIndex.FindByDnsName(None, dnsName=name, vmSearch=True)

    def vm_byIP(self, ip):
        logger.debug('find VM by ip {}'.format(ip))
        return self._ServiceInstance.content.searchIndex.FindByIp(None, ip=ip, vmSearch=True)

    def vm_byUUID(self, uuid):
        logger.debug('find VM by uuid {}'.format(uuid))
        return self._ServiceInstance.content.searchIndex.FindByUuid(None, uuid=uuid, vmSearch=True)

    def vm_byNAME(self, name):
        pass

    def close(self):
        """Gizmo closes VCenter connection """
        Disconnect(self._ServiceInstance)
        self.connected = False
        logger.debug('vcenter disconnected {}'.format(self.__str__()))


class VCenterVMs(object):
    # total VM count
    vm_count = 0

    # support search methods
    _BY_SUPPORT = ['dns', 'ip', 'uuid']

    # def __init__(self, session=None, username=None, password=None, server=None, find=None, by=None, port=443):
    def __init__(self, session, search, by):
        """VMware VCenter guest management
   
        Manage guest VMs in VCenter 
    
        required arguments:
            session:    None    (VCenterConnect): VCenter Connect object
            search:     None    (str): Search string to search VM
            by:         dns     (str): Type of VM search [dns, ip, uuid]
            
        keyword arguments:
    
        Returns:
            gizmo.vmware.VCenterVMs object
        """
        if type(session) is VCenterConnect:
            self.session = session
            logger.info('VCenterConnect found {}'.format(session))
        else:
            ERROR = 'VCenterConnect not found {}'.format(session)
            logger.debug(ERROR)
            raise VCenterConnectError(ERROR)

        if by in self._BY_SUPPORT:
            self.by = by
            logger.debug('by is {}'.format(self.by))
        else:
            ERROR = 'unsupported search by {by} valid({bys})'.format(by=by, bys=', '.join(self._BY_SUPPORT))
            logger.debug(ERROR)
            raise VCenterConnectInvalid(ERROR)

        if search is None:
            raise Exception('no search string passed')
        else:
            self.find = search

        logger.debug('search is {}'.format(self.find))

        VCenterVMs.vm_count += 1
        logger.debug('VCenterVMs {num} {object}'.format(num=self.vm_count, object=self.__str__()))

    def __repr__(self):
        return 'VCenterVMs(username=\'{username}\', password=\'*****\', server=\'{server}\', find=\'{find}\', by=\'{find_by}\', port={port})' \
            .format(username=self.session.username, server=self.session.server, find=self.find, find_by=self.by,
                    port=self.session.port)

    def __str__(self):
        return '<VCenterVMs {username}:{server}:{port}({find_by}:{find})>' \
            .format(username=self.session.username, server=self.session.server, find=self.find, find_by=self.by,
                    port=self.session.port)

    def __enter__(self):
        logger.debug('enter VCenterVMs with')
        return self.find_vm()

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.debug('exit VCenterVMs with')
        self.session.close()

    def power_off_vm(self):
        logger.debug('power off vm {}'.format(self.vm_name))
        return self._VirtualMachine.PowerOff()

    def power_on_vm(self):
        logger.debug('power on vm {}'.format(self.vm_name))
        return self._VirtualMachine.PowerOn()

    def reset_vm(self):
        logger.debug('power reset vm {}'.format(self.vm_name))
        return self._VirtualMachine.Reset()

    # Guest Tools Properties
    @property
    def tools_version(self):
        return self._VirtualMachine.config.tools.toolsVersion

    @property
    def tools_status(self):
        return self._VirtualMachine.guest.toolsStatus

    # Guest Hardware Properties
    @property
    def num_cpu(self):
        return self._VirtualMachine.config.hardware.numCPU

    @property
    def memory_hot_add(self):
        return self._VirtualMachine.config.memoryHotAddEnabled

    @property
    def cpu_hot_add(self):
        return self._VirtualMachine.config.cpuHotAddEnabled

    @property
    def cpu_hot_remove(self):
        return self._VirtualMachine.config.cpuHotRemoveEnabled

    @property
    def num_cores_per_socket(self):
        return self._VirtualMachine.config.hardware.numCoresPerSocket

    @property
    def memory_mb(self):
        return self._VirtualMachine.config.hardware.memoryMB

    @property
    def path_name(self):
        return self._VirtualMachine.summary.config.vmPathName

    @property
    def uuid(self):
        return self._VirtualMachine.config.uuid

    @property
    def boot_time(self):
        return self._VirtualMachine.runtime.bootTime

    @property
    def question(self):
        return self._VirtualMachine.runtime.question

    @property
    def ipaddress(self):
        return self._VirtualMachine.guest.ipAddress

    @property
    def guest_state(self):
        return self._VirtualMachine.guest.guestState

    @property
    def overall_status(self):
        return self._VirtualMachine.summary.overallStatus

    @property
    def guest_name(self):
        return self._VirtualMachine.guest.guestFullName

    @property
    def hostname(self):
        return self._VirtualMachine.guest.hostName

    @property
    def esx_host(self):
        return self._VirtualMachine.runtime.host.name

    @property
    def last_modified(self):
        return self._VirtualMachine.config.modified

    @property
    def vm_name(self):
        return self._VirtualMachine.config.name

    @property
    def vm_state(self):
        return self._VirtualMachine.runtime.powerState

    def power(self, action, **kwargs):
        """VCenter VM actions [on, off, reset]"""

        background = bool(kwargs.get('background', False))
        logger.debug(('background {}'.format(background)))

        power_func = {
            'on': self.power_on_vm,
            'off': self.power_off_vm,
            'reset': self.reset_vm,
        }

        if background:
            return power_func.get(action, 'on')()

        WaitForTask(power_func.get(action, 'on')())

    def find_vm(self):
        self.session.connect()

        get_vm_func = {
            'uuid': self.session.vm_byUUID,
            'dns': self.session.vm_byDNS,
            'ip': self.session.vm_byIP,
        }

        vm = get_vm_func.get(self.by)(self.find)

        if not vm:
            raise Exception('VirtualMachine {}:{} not found'.format(self.by, self.find))

        self._VirtualMachine = vm

        logger.debug('vm object {}'.format(self._VirtualMachine))
        # return get_vm_func.get(self.by, self.vm_byDNS)(self.find)
        return self

    @property
    def snapshots(self):
        return self.vm_snapshot('list')

    def vm_snapshot(self, action, **kwargs):
        """VCenter VM snapshot actions [list, create, remove, revert]"""

        name = kwargs.get('name', None)
        logger.debug(('name {}'.format(name)))

        if action == 'list' and name is None:
            if self._VirtualMachine.snapshot is not None:
                # if type(self._VirtualMachine.snapshot.rootSnapshotList) is list:
                name = self._VirtualMachine.snapshot.rootSnapshotList
            else:
                name = []
            return self.vm_snapshot_list(name)

        snap_func = {
            'create': self.vm_snapshot_create,
            'remove': self.vm_snapshot_remove,
            'revert': self.vm_snapshot_revert,
        }
        return snap_func.get(action, self.vm_snapshot_list)(**kwargs)

    def vm_snapshot_create(self, name, **kwargs):

        logger.debug(('name {}'.format(name)))

        background = bool(kwargs.get('background', False))

        data = dict()
        data['memory'] = bool(kwargs.get('memory', False))
        data['quiesce'] = bool(kwargs.get('quiesce', False))

        logger.debug('create snapshot {}'.format(name))
        if background:
            return self._VirtualMachine.CreateSnapshot(name=name, **data)

        WaitForTask(self._VirtualMachine.CreateSnapshot(name=name, **data))

    def vm_snapshot_remove(self, name, **kwargs):

        logger.debug('remove snapshot {}'.format(name))

        background = bool(kwargs.get('background', False))

        data = dict()
        data['removeChildren'] = bool(kwargs.get('removeChildren', False))

        # get snapshot object
        vm_snap = [snap['object'] for snap in self.vm_snapshot_list(self._VirtualMachine.snapshot.rootSnapshotList) if
                   snap['name'] == name]

        if len(vm_snap) != 1:
            logger.debug('failed to locate snapshot {}'.format(vm_snap))
            raise VCenterConnectInvalid('unable to locate snaphot')

        if background:
            return vm_snap[0].snapshot.RemoveSnapshot_Task(**data)

        WaitForTask(vm_snap[0].snapshot.RemoveSnapshot_Task(**data))

    def vm_snapshot_revert(self, name, **kwargs):
        logger.debug('revert snapshot {}'.format(name))

        background = bool(kwargs.get('background', False))

        # get snapshot object
        vm_snap = [snap['object'] for snap in self.vm_snapshot_list(self._VirtualMachine.snapshot.rootSnapshotList) if
                   snap['name'] == name]

        if len(vm_snap) != 1:
            logger.debug('failed to locate snapshot {}'.format(vm_snap))
            raise VCenterConnectInvalid('unable to locate snaphot')

        if background:
            vm_snap[0].snapshot.RevertToSnapshot_Task()

        WaitForTask(vm_snap[0].snapshot.RevertToSnapshot_Task())

    def vm_snapshot_list(self, snapshotList, parent=None):
        """return list of dicts of snapshots"""
        if parent is None:
            parent = '-'

        snapshots = list()

        for snapshot in snapshotList:
            snapshots.append({
                'parent': parent,
                'id': snapshot.id,
                'name': snapshot.name,
                'state': snapshot.state,
                'description': snapshot.description,
                'createTime': snapshot.createTime,
                'current': snapshot.snapshot == self._VirtualMachine.snapshot.currentSnapshot,
                'object': snapshot,
            })
            if len(snapshot.childSnapshotList) > 0:
                snapshots = snapshots + self.vm_snapshot_list(snapshot.childSnapshotList, parent=snapshot.name)
        # if len(snapshots) < 1:
        #     snapshots.append({
        #         'parent': '-',
        #         'id': 0,
        #         'name': 'No snapshots',
        #         'state': None,
        #         'description': None,
        #         'createTime': datetime(1975, 1, 13),
        #         'current': False,
        #         'object': None,
        #     })
        return snapshots

    @property
    def tasks(self):
        task_list = list()

        for item in self._VirtualMachine.recentTask:
            tdict = {
                'key': item.info.key,
                'name': item.info.name,
                'description': item.info.descriptionId,
                'progress': item.info.progress,
                'state': item.info.state,
                'object': item
            }
            task_list.append(tdict)

        return task_list

    @property
    def alarms(self):
        alarm_list = []

        for item in self._VirtualMachine.triggeredAlarmState:
            tdict = {
                'name': item.key.split('.')[0],
                'status': item.overallStatus
            }
            alarm_list.append(tdict)

        return alarm_list

    def devices(self):
        pass
